package com.colt.novitas.admin.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONObject;

import com.colt.novitas.admin.response.CustomerResponse;
import com.colt.novitas.client.util.Callback;
import com.colt.novitas.client.util.HttpUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class AdminAPIClient {
	
	private static final Gson GSON = new Gson();

	private String ADMIN_API_LOGIN_URL = "http://localhost:8080/admin-api/api/api/login";
	private String ADMIN_API_CUSTOMER_URL = "http://localhost:8080/admin-api/api/customer/ids";

	private static String ADMIN_API_USERNAME = "admin@colt.net";
	private static String ADMIN_API_PASSWORD = "Novitas@123";

	public AdminAPIClient() {
		super();
	}

	public AdminAPIClient(String ADMIN_API_LOGIN_URL, String ADMIN_API_CUSTOMER_URL,String ADMIN_API_USERNAME,String ADMIN_API_PASSWORD ) {
		super();
		this.ADMIN_API_LOGIN_URL = ADMIN_API_LOGIN_URL;
		this.ADMIN_API_CUSTOMER_URL = ADMIN_API_CUSTOMER_URL;
		this.ADMIN_API_USERNAME = ADMIN_API_USERNAME;
		this.ADMIN_API_PASSWORD = ADMIN_API_PASSWORD;
	}

	public String updateCustomerStatus(Integer customerId, String status) {

		String adminToken = getAdminToken();

		if (adminToken != null && !adminToken.equals("")) {

			String url = ADMIN_API_CUSTOMER_URL + "/" + customerId + "?status=" + status;

			HttpClient client = null;
			PutMethod putMethod = null;
			BufferedReader correctResponseReader = null;
			BufferedReader errorResponseReader = null;

			try {

				client = new HttpClient();

				JSONObject jsonParam = new JSONObject();
				jsonParam.put("status", status);

				StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
						"UTF-8");

				// Send PUT request
				putMethod = new PutMethod(url);
				putMethod.setRequestEntity(requestEntity);

				// Add Headers
				putMethod.addRequestHeader("Content-Type", "application/json");
				putMethod.addRequestHeader("Auth-token", adminToken);

				int responseCode = client.executeMethod(putMethod);
				System.out.println("\n Sending 'PUT' request to URL : " + url);
				System.out.println("Response Code : " + responseCode);

				if (responseCode == 200) {
					correctResponseReader = new BufferedReader(
							new InputStreamReader(putMethod.getResponseBodyAsStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = correctResponseReader.readLine()) != null) {
						response.append(inputLine);
					}
					correctResponseReader.close();
					System.out.println(response.toString());
					return "SUCCESS";
				} else if (responseCode == 204) {
					System.out.println("Successfully updated the customer status");
					return "SUCCESS";
				} else {
					errorResponseReader = new BufferedReader(
							new InputStreamReader(putMethod.getResponseBodyAsStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = errorResponseReader.readLine()) != null) {
						response.append(inputLine);
					}
					errorResponseReader.close();

					System.out.println(response.toString());
					return "FAILED";
				}
			}

			catch (Exception e) {
                 return "FAILED";
			} finally {

				try {
					if (correctResponseReader != null) {
						correctResponseReader.close();
					}

					if (errorResponseReader != null) {
						errorResponseReader.close();
					}

				} catch (IOException e) {
					e.printStackTrace();

				}
			}
		}
		
		return "FAILED";
	}

	private String getAdminToken() {

		String token = "";

		String url = ADMIN_API_LOGIN_URL;

		HttpClient client = null;
		PostMethod postMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {
			client = new HttpClient();

			// Set JSON request
			JSONObject jsonParam = new JSONObject();
			jsonParam.put("email", ADMIN_API_USERNAME);
			jsonParam.put("password", ADMIN_API_PASSWORD);

			// Send post request
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
					"UTF-8");

			postMethod = new PostMethod(url);
			postMethod.setRequestEntity(requestEntity);

			// Add Headers
			postMethod.addRequestHeader("Content-Type", "application/json");

			int responseCode = client.executeMethod(postMethod);
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("POST parameters : " + jsonParam);
			System.out.println("Response Code : " + responseCode);

			if (responseCode == 200) {
				correctResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();

				// Print result
				System.out.println("Admin Login Response ------>" + response.toString());

				JSONObject responseJson = new JSONObject(response.toString());
				token = responseJson.get("token").toString();

			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();
				// Print Error result
				System.out.println(response.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}
				if (errorResponseReader != null) {
					errorResponseReader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return token;
	}
	
	public List<CustomerResponse> getAllCustomers() {
		
		String adminToken = getAdminToken();

		if (adminToken == null || adminToken.equals("")) {
			throw new IllegalStateException("Invalid admin token");
		}

        final List<CustomerResponse> results = new ArrayList<CustomerResponse>();
        
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Auth-token", adminToken);

        HttpUtils.connect(ADMIN_API_CUSTOMER_URL, headers, new Callback() {

            @Override
            public void success(String response, int code) {

                List<CustomerResponse> ports = GSON.fromJson(response, new TypeToken<List<CustomerResponse>>(){}.getType());
                results.addAll(ports);
            }

            @Override
            public void failure(String response, int code) { 
            }

            @Override
            public void failure(Exception e) {                
            }

        });

        return results;
    }
}
