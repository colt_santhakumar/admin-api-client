package com.colt.novitas.admin.response;

import com.google.gson.annotations.SerializedName;

public class CustomerResponse {

	@SerializedName("id")
	private Integer id;
	
	@SerializedName("ocn")
	private String ocn;
	
	@SerializedName("bcn")
	private String bcn;
	
	@SerializedName("legal_name")
	private String legalName;
	
	@SerializedName("legal_address")
	private String legalAddress;
	
	@SerializedName("rental_currency")
	private String rentalCurrency;
	
	@SerializedName("discount")
	private Float discountPercentage;
	
	@SerializedName("status")
	private Status status;
	
	@SerializedName("customer_email")
	private String customerEmail;
	
	@SerializedName("telephone_number")
	private String telephoneNumber;
	
	@SerializedName("fax_number")
	private String faxNumber;
	
	@SerializedName("website")
	private String website;
	
	@SerializedName("no_of_users")
	private Integer noOfUsers;
	
	@SerializedName("no_of_services")
	private Integer noOfServices;
	
	@SerializedName("country_code")
	private String countryCode;
	
	@SerializedName("language_code")
	private String languageCode;
	

	public CustomerResponse() {
		super();
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	
	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	
	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	
	public String getLegalAddress() {
		return legalAddress;
	}

	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}

	
	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	
	public Float getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Float discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	
	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	
	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	
	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	
	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	
	public Integer getNoOfUsers() {
		return noOfUsers;
	}

	public void setNoOfUsers(Integer noOfUsers) {
		this.noOfUsers = noOfUsers;
	}

	
	public Integer getNoOfServices() {
		return noOfServices;
	}

	public void setNoOfServices(Integer noOfServices) {
		this.noOfServices = noOfServices;
	}

	
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

}
