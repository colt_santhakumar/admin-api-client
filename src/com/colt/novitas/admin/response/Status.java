package com.colt.novitas.admin.response;

public enum Status {

	ONBOARDING, 
	SIEBEL_ONBOARDING,
	KENAN_ONBOARDING,
	ONBOARD_FAILED, 
	ACTIVE, 
	LOCKED, 
	INACTIVE, 
	ARCHIVED;

}
